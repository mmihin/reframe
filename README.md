Uputstva korištenja sourcetree/bitbucket:

1. Svaki dan počinje i završava na "develop" grani
2. Sa develop grane se povlače promjene ako postoje tj. "pull"
3. Nakon toga se prebacuje na lokanu vlastitu granu npr. "feature/nikola" sa koje se radi dalje
4. Napravi se desni klik na develop granu i odaabere "Merge develop into current branch"
5. Nakon što je merge obavljen napravi se "push" na trenutnu granu npr. "feature/nikola"
6. Nastavi se rad na istoj grani
7. Ako je zadatak gotov napravi se "commit" na istu granu s komentarom npr. "završen styling zaglavlja"
8. Napravi se "push" na istu granu
9. Prebaci se na "develop" granu, napravi se merge sa lokalne grane tj. desni klik miša na "feature/nikola" i "merge feature/nikola into current branch" tj. develop granu
10. Napravi se "push" na develop granu, ako je u međuvremenu netko dodao nešto napravi se "pull"